import React from 'react';
import withStoreProvider from './redux/withStoreProvider'
import DashboardPage from './pages/DashboardPage'
import LoginPage from './pages/LoginPage'
import { Router } from '@reach/router';
import config from './config/firebase'
import * as firebase from "firebase/app";
import "firebase/auth";
import {
  FirebaseAuthProvider,
  FirebaseAuthConsumer,
  IfFirebaseAuthed
} from "@react-firebase/auth";
function App() {
  return (
    <FirebaseAuthProvider {...config} firebase={firebase}>
      <FirebaseAuthConsumer>
        {({ isSignedIn, user, providerId }) => {
          return (
            <Router>
              <DashboardPage path="/dashboard" isSignedIn={isSignedIn} user={user} />
              <LoginPage path="/" isSignedIn={isSignedIn} user={user} />
            </Router>
          );
        }}
      </FirebaseAuthConsumer>
    </FirebaseAuthProvider>
  );
}

export default withStoreProvider(App);
