import firebase from 'firebase'
import "firebase/auth";

export const config = {
  apiKey: "AIzaSyC8RxyvZ0XWC3Q03ylKiL1EfK8V6jAe8sA",
  authDomain: "mobills-react-test.firebaseapp.com",
  databaseURL: "https://mobills-react-test.firebaseio.com",
  projectId: "mobills-react-test",
  storageBucket: "mobills-react-test.appspot.com",
  messagingSenderId: "878664250042",
  appId: "1:878664250042:web:9849991334ffd32006c883"
};
// Initialize Firebase
firebase.initializeApp(config);

export default firebase