import styled from "styled-components";
import { vp } from '../../utils';
import { Divider } from '@material-ui/core'

export const Container = styled.div`
  background-color: #ffffff;

  width: ${vp.dk.vw(540)};
  height: ${vp.dk.vw(920)};
  border-radius: ${vp.dk.vw(22)};
  padding: ${vp.dk.vw(30)};

  @media only screen and (max-width: 1280px){
    width: 360px;
    /* height: 533px; */
    height: 613px;
    border-radius: 14px;
    padding: 20px;
  }

  @media (max-width: 1024px){
    width: 288px;
    /* height: 426px; */
    height: 490px;
    border-radius: 11px;
    padding: 16px;
  }
`

export const TitleWrapper = styled.div`
  display: flex;
  width: 100%;
`

export const DataWrapper = styled.div`
  overflow-y: auto;
  height: 90%;
`

export const Divisor = styled(Divider)`
  margin-top: ${vp.dk.vw(10)};
  margin-bottom: ${vp.dk.vw(10)};

  @media only screen and (max-width: 1280px){
    margin-top: 6px;
    margin-bottom: 6px;
  }

  @media (max-width: 1024){
    margin-top: 5px;
    margin-bottom: 5px;
  }
`

