import React, { useState, useEffect } from 'react'
import { connect, useSelector } from 'react-redux'
import { getAll as getAllExpenses, remove as removeExpense, edit as editExpense } from '../../redux/actions/expenses'
import { getAll as getAllRevenues, remove as removeRevenue, edit as editRevenue } from '../../redux/actions/revenues'

const withConnect = Component => {
  const actions = {
    getAllExpenses,
    getAllRevenues,
    removeRevenue,
    removeExpense,
    editExpense,
    editRevenue
  }
  return connect(
    null,
    actions
  )(Component)
}

export default Component => withConnect(props => {
  const { collection } = props
  
  const data = {
    expenses: useSelector(state => state.expenses.all),
    revenues: useSelector(state => state.revenues.all)
  }

  useEffect(() => {
    if (collection === "expenses") {
      props.getAllExpenses()
    } else {
      props.getAllRevenues()
    }
  }, [])

  const handleDelete = (id) => {
    if (collection === "expenses")
      return props.removeExpense(id)

    return props.removeRevenue(id)
  }

  const handleEdit = (id) => {
    const info = data[collection].filter(record => record.id === id)[0]
    console.log(info)
    if (collection === "expenses")
      return props.editExpense(info)

    return props.editRevenue(info)
    
  }

  return (
    <Component
      onDeleteAction={handleDelete}
      onEditAction={handleEdit}
      type={collection}
      dataSource={data[collection]}
    />
  )
})