import React from 'react'

import {
  List
} from '@material-ui/core'

import {
  Container,
  TitleWrapper,
  Divisor,
  DataWrapper
} from './styles'

import Item from '../_UI/Item'
import Title from '../_UI/Text/Title'
import withLogic from './withLogic'

const ExpensesList = ({ dataSource, onDeleteAction, onEditAction, type, ...restProps }) => {

  return (
    <Container>
      <TitleWrapper>
        <Title>Lista despesas</Title>
      </TitleWrapper>
      <DataWrapper>
        <List>
          {(dataSource || []).map((item, index) => (
            <React.Fragment key={index}>
              <Item
                data={item}
                deleteAction={() => onDeleteAction(item.id)}
                editAction={() => onEditAction(item.id)}
                type={type}
                key={index} />
              {index !== dataSource.length - 1 && <Divisor />}
            </React.Fragment>
          ))}
        </List>
      </DataWrapper>
    </Container>
  )
};

export default withLogic(ExpensesList);