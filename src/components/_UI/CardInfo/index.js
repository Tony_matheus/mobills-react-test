import React from 'react'
import styled, { css } from 'styled-components'
import { vp } from '../../../utils'
import PieChart from '../Chart'

export const Container = styled.div`
  position: relative;
  margin-top: ${vp.dk.vw(20)};
  width: ${vp.dk.vw(360)};
  height: ${vp.dk.vw(320)};
  border-radius: ${vp.dk.vw(22)};
  /* padding: ${vp.dk.vw(29) + " " + vp.dk.vw(22) + " " + vp.dk.vw(30) + " " + vp.dk.vw(22)}; */
  background-color: #ffffff;
  transform: rotate(0deg);
  cursor: pointer;
  transition: all 1s ease-out;

  @media (max-width: 1280px){
    width: 240px;
    height: 262px;
    border-radius: 14px;
  }

  @media (max-width: 1024px){
    width: 192px;
    height: 210px;
    border-radius: 11px;
  }

  ${({ open }) => open &&
    css`
    /* transform: rotate(45deg); */
    position: fixed;
    top: 2.5%;
    left: 2.5%;
    bottom: 2.5%;
    right: 2.5%;
    z-index: 4;
    width: auto;
    height: auto;
  `
  }
`

const CardInfo = () => {
  const [visible, setVisible] = React.useState(false)
  return (
    <Container open={visible} onClick={() => setVisible(!visible)}>
      <PieChart />
    </Container>
  )
}

export default CardInfo