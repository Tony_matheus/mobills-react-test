import styled from 'styled-components'

export const Text = styled.p`
  font-size: 12px;
  font-weight: normal;
  letter-spacing: normal;
  text-align: left;
  color: #333333;
  margin: 0;
`