import React from 'react'
import styled from 'styled-components'
import { vp } from '../../../utils'

export const Container = styled.div`
  width: ${vp.dk.vw(258)};
  height: ${vp.dk.vw(87)};
  padding: ${vp.dk.vw(18)+" 0px " + vp.dk.vw(18)+" "+vp.dk.vw(40)};
  border-radius: ${vp.dk.vw(17)};
  /* border-top-left-radius: ${vp.dk.vw(17)}; */
  /* border-bottom-left-radius: ${vp.dk.vw(17)}; */
  background-color: #ffffff; 
  /* position: fixed; */
  flex-direction: column;
  margin-bottom: ${vp.dk.vw(20)};
`

export const Title = styled.p`
  font-size: 16px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.13;
  letter-spacing: normal;
  color: #333333;
  margin-bottom: ${vp.dk.vw(10)};
`

export const Info = styled.p`
  font-size: 20px;
  font-weight: bold;
  font-style: normal;
  letter-spacing: normal;
  line-height: 1.25;
  color: #44d144;
`

const CardSide = ({ title, value}) => {
  return(
    <Container>
      <Title>{title}</Title>
      <Info>{`R$ ${value}`}</Info>
    </Container>
  )
}

export default CardSide;