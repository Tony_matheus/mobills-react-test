import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
  List,
  ListItem,
  Divider,
  ListItemText,
  ListItemAvatar,
  IconButton,
  ListItemSecondaryAction,
  Avatar
} from '@material-ui/core'

import ReceiptIcon from '@material-ui/icons/Receipt'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import {
  Wrapper,
  Column,
  Text,
  ActionWrapper,
  TextValue
} from './styles'

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

const Item = ({ data, deleteAction, editAction, type = "expenses", ...restProps }) => {
  const {
    value,
    paidOut,
    // type,
    description,
    date,
    recieved
  } = data


  return (
    <ListItem alignItems="flex-start" {...restProps} >
      <ListItemAvatar>
        <Avatar>
          <ReceiptIcon />
        </Avatar>
      </ListItemAvatar>
      <ListItemText
        primary={description}
        secondary={
          <>
            <Wrapper>
              <TextValue type={type}>
                {type === "expenses" ? "-" : "+"}R${value}
              </TextValue>
              <Text>
                {date || data.data}
              </Text>
              {
                type === "expenses"
                  ?
                  <Text>
                    {paidOut ? "Pago" : "Pendente"}
                  </Text>
                  :
                  <Text>
                    {recieved ? "Recebido" : "Pendente"}
                  </Text>
              }
            </Wrapper>
          </>
        }
      />
      <ListItemSecondaryAction>
        <ActionWrapper>
          <IconButton
            onClick={deleteAction}
            edge="end"
            aria-label="deletar"
          >
            <DeleteIcon />
          </IconButton>
          <IconButton
            onClick={editAction}
            edge="end"
            aria-label="editar"
          >
            <EditIcon />
          </IconButton>
        </ActionWrapper>
      </ListItemSecondaryAction>
    </ListItem>
  )
}

export default Item