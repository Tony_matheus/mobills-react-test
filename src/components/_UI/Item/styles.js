import styled from 'styled-components'
import { vp } from '../../../utils'

export const Wrapper = styled.span`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`

export const Column = styled.span`
  flex-direction: column;
`

export const Text = styled.span`
  font-size: 1rem;
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  font-weight: 400;
  line-height: 1.5;
  letter-spacing: 0.00938em;
`
export const ActionWrapper = styled.div`
  display: flex;
  flex-direction: column;
`


export const TextValue = styled(Text)`
  ${({ type }) => type === "expenses"
    ? `
    color: #d04f4c;
  `
    : `
    color: #4b9834;
  `}
`