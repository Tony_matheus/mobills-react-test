import React from 'react'
import { storiesOf } from '@storybook/react'
import { withKnobs, text, select } from '@storybook/addon-knobs'

import Item from './'

storiesOf('UI|Item/Item', module)
  .addDecorator(withKnobs)
  .add('default', () =>
    <Item />
  )
