import React from 'react'
import styled from 'styled-components'

export const Overlay = styled.div`
  position: fixed;
  top:0;
  left:0;
  right:0;
  bottom:0;
  background: #333;
  z-index: 3;
`

export const Spin = styled.span`
  position: fixed;
  width: 100px;
  height: 100px;
  background: #d8d8d8;
  z-index: 10;
  display: block;
  border-radius: 20px;
  top: calc(50% - 50px);
  right: calc(50% - 50px);
  animation: pulse 2s infinite ease-in-out ;

  @keyframes pulse {
  0%{
    border-raidius:50%;
    transform:scale(1) rotate(-360deg);
  }
  50%{
    border-radius:5%;
    background: #da3b3b;
    transform:scale(1.2) rotate(0deg);
  }
  100%{
    border-radius:50%;
    transform:scale(1) rotate(360deg);
  }
}
`

export const Visible = styled.span`
  ${({visible}) => visible || 'display: none;'}
`

const Loading = ({visible}) => {
  return (
    <Visible visible={visible}>
      <Overlay />
      <Spin/>
    </Visible>
  )
}

export default Loading