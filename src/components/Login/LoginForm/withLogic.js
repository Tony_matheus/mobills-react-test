import React, { useState, useEffect } from 'react';
import { useSelector, connect } from 'react-redux'
import { navigate } from '@reach/router';
import { auth, logout } from '../../../redux/actions/user';
import { message } from 'antd'

const withConnect = Component => {
  return connect(null, {
    auth,
    logout
  })(Component);
}

export default Component => withConnect(props => {

  const error = useSelector(state => state.user.error)

  const [state, setState] = useState({
    email: 'teste@teste.com',
    password: '12345678'
  });

  useEffect(() => {
    if (error) {
      message.error(error)
    }
  }, [error])




  const onChange = (event) => {
    setState({
      ...state,
      [event.target.name]: event.target.value
    })
  }

  const handleSubmit = () => {
    props.auth(state)
  };

  const handleLogout = () => {
    props.logout(state)
  };

  return (
    <Component
      onSubmit={handleSubmit}
      handleLogout={handleLogout}
      onChange={onChange}
      authenticating={false}
      state={state}
    />
  )
})
