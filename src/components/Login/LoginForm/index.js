import React, { useState, useEffect } from 'react';
import { useSelector, connect } from 'react-redux'
import { navigate } from '@reach/router';
import withLogic from './withLogic'
import { Button, Input, Form, Icon } from 'antd';
import { FormContainer, Title, InputContainer, LoginButton, Text, TextLink } from './styles';

const LoginFields = ({ onSubmit, onChange, authenticating, handleLogout, state, ...restProps }) => {

  const { getFieldDecorator, validateFields } = restProps.form;

  const handleSubmit = (e) => {
    e.preventDefault()
    validateFields((err, values) => {
      if (!err) {
        onSubmit()
      }
    })
  }

  return (
    <FormContainer>

      <Title> Comece a organizar suas finanças </Title>

      <Form onSubmit={handleSubmit}>
        <InputContainer>
          <Form.Item>
            {getFieldDecorator('E-mail', {
              initialValue: state.email,
              setFieldsValue: state.email,
              rules: [{ required: true, message: 'Por favor coloque seu email!' }],
            })(
              <Input
                prefix={<Icon type='mail'
                  style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder='E-mail'
                onChange={onChange}
                name='email'
              />,
            )}
          </Form.Item>
        </InputContainer>

        <InputContainer>
          <Form.Item>
            {getFieldDecorator('password', {
              initialValue: state.password,
              setFieldsValue: state.password,
              rules: [{ required: true, message: 'Por favor coloque sua senha!' }],
            })(
              <Input.Password
                prefix={<Icon type='lock' style={{
                  color: 'rgba(0,0,0,.25)'
                }} />}
                placeholder='Password'
                onChange={onChange}
                name='password'
              />,
            )}
          </Form.Item>
        </InputContainer>

        <InputContainer>
          <LoginButton
            type='primary'
            htmlType='submit'
            loading={authenticating}
          >
            Login
          </LoginButton>
          {/* <Text>New in here? <TextLink onClick={() => navigate("/register")}> Sign Up! </TextLink> </Text> */}
        </InputContainer>
      </Form>
    </FormContainer>
  )
}

const LoginForm = Form.create()(LoginFields);

export default withLogic(LoginForm);