import React, { useEffect, useState } from 'react';
import { useSelector, connect } from 'react-redux'
import { Container } from './styles';
import { message } from 'antd';
import LoginForm from './LoginForm';

const Login = (props) => {

    return (
        <Container>
            <LoginForm />
        </Container>
    )
}

export default Login