import React from 'react'
import CardSide from '../_UI/CardSide'
import withLogic from './withLogic';

const DataInfo = ({ dataInfo, ...restProps }) => {
  return (
    <>
      {dataInfo.map((data, index) => (
        <CardSide
          title={data.title}
          value={data.value}
        />
      ))}
    </>
  )
}

export default withLogic(DataInfo)