import React, { useState, useEffect } from 'react'
import { connect, useSelector } from 'react-redux'


const withConnect = Component => {
  const actions = {
  }
  return connect(
    null,
    actions
  )(Component)
}

export default Component => withConnect(props => {
  const { collection } = props

  const { paidOut, notPaidOut } = useSelector(state => state.expenses)
  const { received, notReceived } = useSelector(state => state.revenues)

  const [dataInfo, setDataInfo] = useState([
    {
      title: "Pago",
      value: paidOut
    },
    {
      title: "Não Pago",
      value: notPaidOut
    },
    {
      title: "Recebido",
      value: received
    },
    {
      title: "Recebido",
      value: notReceived
    }
  ])

  useEffect(() => {
    setDataInfo([
      {
        title: "Pago",
        value: paidOut
      },
      {
        title: "Não Pago",
        value: notPaidOut
      },
      {
        title: "Recebido",
        value: received
      },
      {
        title: "Recebido",
        value: notReceived
      }
    ])
  }, [paidOut, notPaidOut, received, notReceived])


  return (
    <Component
      dataInfo={dataInfo}
    />
  )
})