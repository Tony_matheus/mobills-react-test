import React from 'react'
import { Field, reduxForm } from 'redux-form'
import { TextField, Checkbox, Button, Divider, FormControlLabel } from '@material-ui/core';
import { DatePicker } from 'antd';
import moment from 'moment';
import renderDatePicker from './DatePicker'
import withLogic from './withLogic'
import {
  Container,
  FormWrapper,
  Column,
  FieldInput,
  CheckColumn,
  CheckText,
  CheckWrapper,
  FormContainer,
  SwitchContainer,
  SwitchButton,
  SubmitButton,
  SubmitWrapper,
  FieldsContainer
} from './styles'

import Title from '../_UI/Text/Title'

const validate = values => {
  const errors = {}
  if (!values.value) {
    errors.value = 'Necessario'
  } else if (values.value.length < 1) {
    errors.value = 'Não pode ser menor que zero'
  }
  if (!values.date) {
    errors.date = 'Necessario'
  }
  if (!values.description) {
    errors.description = 'Necessario'
  }

  return errors
}

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => (
    <div>
      <label>{label}</label>
      <div>
        <input {...input} placeholder={label} type={type} />
        {touched &&
          ((error && <span>{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    </div>
  )


const renderCheckbox = ({ input }) => (
  <div>
    <FormControlLabel
      control={
        <Checkbox
          checked={input.value ? true : false}
          onChange={input.onChange}
        />
      }
    />
  </div>
)


const FinancialMovements = (props) => {

  const {
    onSubmit,
    handleSubmit,
    expensesVisible,
    setExpensesVisible,
    pristine,
    reset,
    loading,
    initialize,
    isEditing,
    initialValues,
    created,
    edited,
    ...restProps
  } = props

  React.useEffect(() => {
    initialize({ checked: false })
  }, [])

  React.useEffect(() => {
    if (isEditing)
      initialize(initialValues)
  }, [isEditing])

  React.useEffect(() => {
    if (created || edited)
      initialize({
        checked: false
      })
  }, [created, edited])

  return (
    <Container>
      <Title>
        {isEditing ? "Editar" : "Adicionar"}
      </Title>
      <SwitchContainer>
        <SwitchButton
          onClick={() => setExpensesVisible(!expensesVisible)}
          variant="contained"
          color={expensesVisible ? "secondary" : ""}
        >
          Despesa
        </SwitchButton>
        <SwitchButton
          onClick={() => setExpensesVisible(!expensesVisible)}
          variant="contained"
          color={!expensesVisible ? "secondary" : ""}
        >
          Receita
        </SwitchButton>
      </SwitchContainer>
      <Divider />
      <FormContainer onSubmit={handleSubmit}>
        <FieldsContainer>
          <FormWrapper>
            <Column>
              <FieldInput
                name="value"
                component="input"
                type="text"
                placeholder="Valor"
              />
            </Column>
          </FormWrapper>
          <FormWrapper>
            <Column>
              <FieldInput
                name="date"
                component={renderDatePicker}
                type="text"
                placeholder="Data"
              />
            </Column>
          </FormWrapper>
          <FormWrapper>
            <Column>
              <FieldInput
                name="description"
                component="input"
                type="text"
                placeholder="Descrição"
              />
            </Column>
          </FormWrapper>
          <CheckWrapper>
            <Field
              name="checked"
              id="checked"
              //component="input"
              component={renderCheckbox}
              type="checkbox"
            />
            <CheckColumn>
              <CheckText>
                {expensesVisible ? "Essa conta já foi paga" : "Já recebi esse valor"}
              </CheckText>
            </CheckColumn>
          </CheckWrapper>
        </FieldsContainer>
        <Divider />
        <SubmitWrapper>
          <SubmitButton variant="contained" color="primary" type="submit" disabled={pristine || loading}>
            {isEditing ? "Editar" : "Adicionar"} {expensesVisible ? "Despesa" : "Receita"}
          </SubmitButton>
        </SubmitWrapper>
      </FormContainer>
    </Container>
  )
}
const FinancialMovementsForm = reduxForm({
  values: {
    value: "dasydg",
    checked: false
  },
  validate,
  form: 'simple' // a unique identifier for this form
})(FinancialMovements)

export default withLogic(FinancialMovementsForm)
