import React, { useState, useEffect } from 'react'
import {message} from 'antd'
import { connect, useSelector } from 'react-redux'
import { create as createExpense, update as updateExpense } from '../../redux/actions/expenses'
import { create as createRevenue, update as updateRevenue } from '../../redux/actions/revenues'
import { load as loadData } from '../../redux/reducers/expenses'
const withConnect = Component => {
  const actions = {
    createExpense,
    createRevenue,
    updateExpense,
    updateRevenue
  }
  return connect(
    null,
    actions
  )(Component)
}

export default Component => withConnect(props => {

  const [expensesVisible, setExpensesVisible] = useState(true)
  const initialValues = useSelector(state => state.expenses.data)

  const isEditingExpense = useSelector(state => state.expenses.isEditing)
  const isEditingRevenue = useSelector(state => state.revenues.isEditing)

  const expenseCreated = useSelector(state => state.expenses.created)
  const revenueCreated = useSelector(state => state.revenues.created)

  const expenseEdited = useSelector(state => state.expenses.edited)
  const revenueEdited = useSelector(state => state.revenues.edited)

  useEffect(() => {
    if (expenseCreated || revenueCreated)
      message.success("adicionado")
  }, [expenseCreated, revenueCreated])


  useEffect(() => {
    if (isEditingExpense)
      setExpensesVisible(isEditingExpense)
    if (isEditingRevenue)
      setExpensesVisible(!isEditingRevenue)
  }, [isEditingExpense, isEditingRevenue])

  useEffect(() => {
    console.warn(initialValues)
  }, [initialValues])

  const handleSubmit = (data) => {
    if (isEditingExpense || isEditingRevenue)
      return handleUpdate(data)

    if (expensesVisible) {
      return props.createExpense({ ...data, paidOut: data.checked })
    }
    return props.createRevenue({ ...data, received: data.checked })

  }

  const handleUpdate = (data) => {
    if (expensesVisible) {
      return props.updateExpense({ ...data, paidOut: data.checked })
    }
    return props.updateRevenue({ ...data, received: data.checked })
  }



  return (
    <Component
      onSubmit={handleSubmit}
      expensesVisible={expensesVisible}
      setExpensesVisible={setExpensesVisible}
      isEditing={isEditingExpense || isEditingRevenue}
      initialValues={initialValues}
      created={expenseCreated || revenueCreated}
      edited={expenseEdited || revenueEdited}
    />
  )
})