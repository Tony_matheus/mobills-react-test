import styled from 'styled-components'
import { Button } from '@material-ui/core';
import { Field } from 'redux-form'
import { vp } from '../../utils'
import { Text } from '../_UI/Text'
import { DatePicker } from 'antd';


export const Container = styled.div`
  width: ${vp.dk.vw(360)};
  min-height: ${vp.dk.vw(394)};
  border-radius: ${vp.dk.vw(22)};
  padding: ${vp.dk.vw(29) + " " + vp.dk.vw(22) + " " + vp.dk.vw(30) + " " + vp.dk.vw(22)};
  background-color: #ffffff;

  @media (max-width: 1280px){
    width: 240px;
    min-height: 262px;
    border-radius: 14px;
  }

  @media (max-width: 1024px){
    width: 192px;
    min-height: 210px;
    border-radius: 11px;
  }
`
export const SwitchContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: ${vp.dk.vw(20)};

  @media only screen and (max-width: 1280px){
    margin-bottom: 13px;
  }
`

export const SwitchButton = styled(Button)`
  width: 45%;

  &.MuiButton-containedSecondary{
    background-color: #d04f4c;
  }
`

export const FormContainer = styled.form`
  
`

export const FormWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`

export const Column = styled.div`
  padding-top: 11px;
  padding-bottom: 11px;
  width: 100% ;
`

export const FieldInput = styled(Field)`
  width: 100% ;
  border-radius: 6px ;
  border: solid 1px #000 ;
  background-color: #ffffff ;
  color: #000 ;
  padding: ${vp.dk.vw(11)};

  @media (max-width: 1280px){
    width: 73px;
    padding: 7px;
  }

  @media (max-width: 1024px){
    width: 58px;
    padding: 5px;
  }
`

export const CheckWrapper = styled.div`
  display: inline-flex;
`
export const CheckColumn = styled(Column)`
  justify-content: flex-end;
`

export const CheckText = styled(Text)`
  margin-left: 10px;
`

export const SubmitWrapper = styled.div`
  margin-top: ${vp.dk.vw(20)};

  @media only screen and (max-width: 1280px){
    margin-top: 13px;
  }
`

export const FieldsContainer = styled.div`
  margin-top: ${vp.dk.vw(10)};
  margin-bottom: ${vp.dk.vw(10)};

  @media only screen and (max-width: 1280px){
    margin-top: 6px;
    margin-bottom: 6px;
  }
`

export const SubmitButton = styled(Button)`
  width: 100%;

  &.MuiButton-containedPrimary{
    background-color: #333;

    &:hover{
      background-color: #222;
    }
  }
`

export const CustomDatepicker = styled(DatePicker)`
  &.ant-calendar-picker{
    width: 100% !important;
    border-radius: 6px !important;
    border: solid 1px #000 !important;
    background-color: #ffffff !important;
    color: #000 !important;
    padding: ${vp.dk.vw(5)}!important;

    @media (max-width: 1280px){
      width: 73px!important;
      padding: 7px!important;
    }

    @media (max-width: 1024px){
      width: 58px!important;
      padding: 5px!important;
    }

    .ant-calendar-picker-input{
      border: 0px;
    }
  }
`
