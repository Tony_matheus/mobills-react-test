import React from 'react'
import moment from 'moment';
import { CustomDatepicker } from './styles'


const renderDatePicker = ({ input }) => {
  return (
    <CustomDatepicker
      value={moment(input.value, "DD/MM/YYYY")}
      format="DD/MM/YYYY"
      onChange={(date, dateString) => { input.onChange(dateString) }}
    />
  )
}

export default renderDatePicker