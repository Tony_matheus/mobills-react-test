
import React from 'react'
import {
  TitleWrapper,
  TitlePage,
  LogoutButton,
} from './styles'
import withLogic from './withLogic'

const TitleBar = ({ onLogout }) => {
  return (
    <TitleWrapper>
      <TitlePage>MovimentApp</TitlePage>
      <LogoutButton
        variant="contained"
        color={"secondary"}
        onClick={onLogout}
      >
        Logout
      </LogoutButton>
    </TitleWrapper>
  )
}

export default withLogic(TitleBar);