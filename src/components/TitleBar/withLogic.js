import React, { useState, useEffect } from 'react';
import { connect, useSelector } from 'react-redux';
import { logout } from '../../redux/actions/user';

const withConnect = Component => {
  const actions = {
    logout
  }
  return connect(
    null,
    actions
  )(Component)
}

export default Component => withConnect(props => {

  const handleLogout = () => {
    props.logout();
  };

  return (
    <Component
      onLogout={handleLogout}
    />
  )
})
