import styled from 'styled-components';
import { Title } from '../_UI/Text/Title'
import { Button } from '@material-ui/core';

export const TitleWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 30px;
`
export const TitlePage = styled(Title)`
  color: white;
`

export const LogoutButton = styled(Button)`

`