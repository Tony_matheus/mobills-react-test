import firebase from '../../../config/firebase'

import {
  REVENUES_UPDATE,
  REVENUES_UPDATE_SUCCESS,
  REVENUES_UPDATE_FAILURE
} from '../../actionTypes';

export default (expense) => {
  return async (dispatch) => {
    try {
      dispatch({ type: REVENUES_UPDATE })
      console.log(expense, "update")
      const db = firebase.firestore()
      const data = await db.collection('expenses').doc(expense.id).set({ ...expense })

      console.log(data, "data")
      dispatch({
        type: REVENUES_UPDATE_SUCCESS,
        payload: expense
      })
    }
    catch (err) {
      console.error(err)
      const errorMessage = "error"
      dispatch({
        type: REVENUES_UPDATE_FAILURE,
        payload: errorMessage
      })
    }
  }
}
