import firebase from '../../../config/firebase'

import {
  REVENUES_DELETE,
  REVENUES_DELETE_SUCCESS,
  REVENUES_DELETE_FAILURE
} from '../../actionTypes';

export default (id) => {
  return async (dispatch) => {
    try {
      dispatch({ type: REVENUES_DELETE })


      const db = firebase.firestore()
      await db.collection('revenues').doc(id).delete()

      dispatch({
        type: REVENUES_DELETE_SUCCESS,
        payload: id
      })
    }
    catch (err) {
      const errorMessage = "error"
      dispatch({
        type: REVENUES_DELETE_FAILURE,
        payload: errorMessage
      })
    }
  }
}
