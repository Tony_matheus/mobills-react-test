import firebase from '../../../config/firebase'

import {
  REVENUES_ADD,
  REVENUES_ADD_SUCCESS,
  REVENUES_ADD_FAILURE
} from '../../actionTypes';

export default (body) => {
  return async (dispatch) => {
    try {
      dispatch({ type: REVENUES_ADD })
      
      const db = firebase.firestore()
      const data = await db.collection("revenues").add({...body})
      console.log(data, "data")
      
      dispatch({
        type: REVENUES_ADD_SUCCESS,
        payload: body
      })
    } 
    catch (err) {
      console.error(err)
      const errorMessage = "error"
      dispatch({
        type: REVENUES_ADD_FAILURE,
        payload: errorMessage
      })
    }
  }
}
