import {
  REVENUES_EDIT
} from '../../actionTypes';

export default (body) => {
  return async (dispatch) => {
    
    dispatch({
      type: REVENUES_EDIT,
      payload: body
    })
  }
}
