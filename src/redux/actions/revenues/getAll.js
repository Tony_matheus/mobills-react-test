import firebase from '../../../config/firebase'

import {
  REVENUES_GET_ALL,
  REVENUES_GET_ALL_SUCCESS,
  REVENUES_GET_ALL_FAILURE
} from '../../actionTypes';

export default (name) => {
  return async (dispatch) => {
    try {
      dispatch({ type: REVENUES_GET_ALL })
      
      const db = firebase.firestore()
      const data = await db.collection("revenues").get()
      
      dispatch({
        type: REVENUES_GET_ALL_SUCCESS,
        payload: data
      })
    } 
    catch (err) {
      console.error(err)
      const errorMessage = "error"
      dispatch({
        type: REVENUES_GET_ALL_FAILURE,
        payload: errorMessage
      })
    }
  }
}
