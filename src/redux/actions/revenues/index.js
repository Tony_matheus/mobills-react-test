import getAll from './getAll'
import remove from './remove'
import create from './create'
import edit from './edit'
import update from './update'

export {
  getAll,
  remove,
  create,
  edit,
  update
}