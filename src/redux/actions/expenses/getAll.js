import firebase from '../../../config/firebase'

import {
  EXPENSES_GET_ALL,
  EXPENSES_GET_ALL_SUCCESS,
  EXPENSES_GET_ALL_FAILURE
} from '../../actionTypes';

export default (name) => {
  return async (dispatch) => {
    try {
      dispatch({ type: EXPENSES_GET_ALL })
      
      const db = firebase.firestore()
      const data = await db.collection("expenses").get()
      console.warn(data.docs, "data")
      
      dispatch({
        type: EXPENSES_GET_ALL_SUCCESS,
        payload: data
      })
    } 
    catch (err) {
      console.error(err)
      const errorMessage = "error"
      dispatch({
        type: EXPENSES_GET_ALL_FAILURE,
        payload: errorMessage
      })
    }
  }
}
