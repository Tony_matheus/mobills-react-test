import firebase from '../../../config/firebase'

import {
  EXPENSES_ADD,
  EXPENSES_ADD_SUCCESS,
  EXPENSES_ADD_FAILURE
} from '../../actionTypes';

export default (body) => {
  return async (dispatch) => {
    try {
      dispatch({ type: EXPENSES_ADD })
      
      const db = firebase.firestore()
      const data = await db.collection("expenses").add({...body})
      console.log(data, "data")
      
      dispatch({
        type: EXPENSES_ADD_SUCCESS,
        payload: body
      })
    } 
    catch (err) {
      console.error(err)
      const errorMessage = "error"
      dispatch({
        type: EXPENSES_ADD_FAILURE,
        payload: errorMessage
      })
    }
  }
}
