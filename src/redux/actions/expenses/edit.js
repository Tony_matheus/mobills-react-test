import {
  EXPENSES_EDIT
} from '../../actionTypes';

export default (body) => {
  return async (dispatch) => {
    
    dispatch({
      type: EXPENSES_EDIT,
      payload: body
    })
  }
}
