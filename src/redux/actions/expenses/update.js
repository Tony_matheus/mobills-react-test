import firebase from '../../../config/firebase'

import {
  EXPENSES_UPDATE,
  EXPENSES_UPDATE_SUCCESS,
  EXPENSES_UPDATE_FAILURE
} from '../../actionTypes';

export default (expense) => {
  return async (dispatch) => {
    try {
      dispatch({ type: EXPENSES_UPDATE })
      console.log(expense, "update")
      const db = firebase.firestore()
      const data = await db.collection('expenses').doc(expense.id).set({ ...expense })

      console.log(data, "data")
      dispatch({
        type: EXPENSES_UPDATE_SUCCESS,
        payload: expense
      })
    }
    catch (err) {
      console.error(err)
      const errorMessage = "error"
      dispatch({
        type: EXPENSES_UPDATE_FAILURE,
        payload: errorMessage
      })
    }
  }
}
