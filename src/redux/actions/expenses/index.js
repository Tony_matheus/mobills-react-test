import getAll from './getAll'
import create from './create'
import edit from './edit'
import remove from './remove'
import update from './update'

export {
  getAll,
  remove,
  create,
  edit,
  update
}