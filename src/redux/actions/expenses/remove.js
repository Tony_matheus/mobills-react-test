import firebase from '../../../config/firebase'

import {
  EXPENSES_DELETE,
  EXPENSES_DELETE_SUCCESS,
  EXPENSES_DELETE_FAILURE
} from '../../actionTypes';

export default (id) => {
  return async (dispatch) => {
    try {
      dispatch({ type: EXPENSES_DELETE })


      const db = firebase.firestore()
      await db.collection('expenses').doc(id).delete()

      dispatch({
        type: EXPENSES_DELETE_SUCCESS,
        payload: id
      })
    }
    catch (err) {
      console.error(err)
      const errorMessage = "error"
      dispatch({
        type: EXPENSES_DELETE_FAILURE,
        payload: errorMessage
      })
    }
  }
}
