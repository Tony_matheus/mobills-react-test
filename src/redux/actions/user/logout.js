import firebase from 'firebase'
import {
  LOGIN_SUCESS,
  LOGIN_ERROR
} from '../../actionTypes'

export default () => {
  return async (dispatch) => {
    try {
      
      const response = await firebase.auth().signOut()
      console.log(response)
      dispatch({ type: LOGIN_SUCESS })
    }
    catch (err) {
      console.error(err)
      dispatch({ type: LOGIN_ERROR, err })
    }
  }
}
