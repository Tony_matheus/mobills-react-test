import firebase from 'firebase'
import {
  USER_AUTHENTICATE,
  USER_AUTHENTICATE_SUCCESS,
  USER_AUTHENTICATE_FAILURE
} from '../../actionTypes'

export default (credentials) => {
  return async (dispatch) => {
    try {
      dispatch({ type: USER_AUTHENTICATE })

      const response = await firebase.auth().signInWithEmailAndPassword(credentials.email, credentials.password)
      console.log(response, "www")
      dispatch({
        type: USER_AUTHENTICATE_SUCCESS,
        payload: ""
      })
    }
    catch (err) {
      console.error(err)
      dispatch({
        type: USER_AUTHENTICATE_FAILURE,
        payload: "Usuário ou senha incorretos"
      })
    }
  }
}
