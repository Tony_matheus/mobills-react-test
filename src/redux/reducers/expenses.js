import { keyBy, sortBy } from 'lodash'


import {
  EXPENSES_GET_ALL,
  EXPENSES_GET_ALL_SUCCESS,
  EXPENSES_GET_ALL_FAILURE,
  EXPENSES_ADD,
  EXPENSES_ADD_SUCCESS,
  EXPENSES_ADD_FAILURE,
  EXPENSES_EDIT,
  EXPENSES_UPDATE,
  EXPENSES_UPDATE_SUCCESS,
  EXPENSES_UPDATE_FAILURE,
  EXPENSES_DELETE,
  EXPENSES_DELETE_SUCCESS,
  EXPENSES_DELETE_FAILURE,
  LOAD
} from '../actionTypes';

const initialState = {
  isRequesting: false,
  isEditing: false,
  created: false,
  edited: false,
  paidOut: 0,
  notPaidOut: 0,
  data: {},
  all: [],
  allById: {},
  error: ''
}

function calcPaidOut(data) {
  const response = data.reduce((sum, record) => {
    console.log(record)
    const value = parseFloat(record.value)
    if (record.paidOut)
      return {
        ...sum,
        paid: sum.paid + value
      }
    return {
      ...sum,
      nPaid: sum.nPaid + value
    }
  }, { paid: 0, nPaid: 0 })
  return response
}

export default function (state = initialState, action) {
  switch (action.type) {
    case EXPENSES_EDIT: {
      console.log(action.payload)
      return {
        ...state,
        isEditing: true,
        data: { ...action.payload, checked: action.payload.paidOut },
        error: ''
      }
    }
    case EXPENSES_UPDATE: {
      return {
        ...state,
        isRequesting: true,
        isEditing: false,
        error: ''
      }
    }
    case EXPENSES_UPDATE_SUCCESS: {
      const data = state.all.filter(t => t.id !== action.payload.id)
      console.log(data)
      return {
        ...state,
        isRequesting: false,
        edited: true,
        all: [
          ...data,
          action.payload
        ]
      }
    }
    case EXPENSES_UPDATE_FAILURE: {
      return {
        ...state,
        isRequesting: false,
        error: action.payload
      }
    }
    case EXPENSES_GET_ALL: {
      return {
        ...state,
        isRequesting: true,
        error: ''
      }
    }
    case EXPENSES_GET_ALL_SUCCESS: {
      const data = action.payload.docs.map(doc => ({ ...doc.data(), id: doc.id }))
      const info = calcPaidOut(data)
      console.log(info)
      return {
        ...state,
        isRequesting: false,  
        paidOut: info.paid,
        notPaidOut: info.nPaid,
        all: data,
        allById: keyBy(data, 'id')
      }
    }
    case EXPENSES_GET_ALL_FAILURE: {
      return {
        ...state,
        isRequesting: false,
        error: action.payload
      }
    }
    case EXPENSES_ADD: {
      return {
        ...state,
        isRequesting: true,
        error: ''
      }
    }
    case EXPENSES_ADD_SUCCESS: {
      const data = [...state.all, action.payload]
      return {
        ...state,
        isRequesting: false,
        created: true,
        all: data,
        allById: keyBy(data, 'id')
      }
    }
    case EXPENSES_ADD_FAILURE: {
      return {
        ...state,
        isRequesting: false,
        error: action.payload
      }
    }
    case EXPENSES_DELETE: {
      return {
        ...state,
        isRequesting: true,
        error: ''
      }
    }
    case EXPENSES_DELETE_SUCCESS: {
      const expenses = state.all.filter(t => t.id !== action.payload)
      return {
        ...state,
        isRequesting: false,
        all: expenses,
        allById: keyBy(expenses, 'id')

      }
    }
    case EXPENSES_DELETE_FAILURE: {
      return {
        ...state,
        isRequesting: false,
        error: action.payload
      }
    }
    default:
      return state
  }
}

export const load = data => ({ type: LOAD, data });