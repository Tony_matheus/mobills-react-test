

import {
  USER_AUTHENTICATE,
  USER_AUTHENTICATE_SUCCESS,
  USER_AUTHENTICATE_FAILURE,
} from '../actionTypes'

const initialState = {
  authenticating: false,
  isAuthenticated: true,
  error: ''
}

export default function (state = initialState, action) {
  switch (action.type) {
    case USER_AUTHENTICATE: {
      return {
        ...state,
        authenticating: true,
        isAuthenticated: false,
        error: ''
      }
    }
    case USER_AUTHENTICATE_FAILURE: {
      return {
        ...state,
        authenticating: false,
        error: action.payload
      }
    }
    case USER_AUTHENTICATE_SUCCESS: {
      return {
        ...state,
        authenticating: false,
        isAuthenticated: true,
        id: action.payload
      }
    }
    default:
      return state
  }
  return state
}