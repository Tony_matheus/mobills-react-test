import { keyBy, sortBy } from 'lodash'


import {
  REVENUES_GET_ALL,
  REVENUES_GET_ALL_SUCCESS,
  REVENUES_GET_ALL_FAILURE,
  REVENUES_ADD,
  REVENUES_ADD_SUCCESS,
  REVENUES_ADD_FAILURE,
  REVENUES_EDIT,
  REVENUES_UPDATE,
  REVENUES_UPDATE_SUCCESS,
  REVENUES_UPDATE_FAILURE,
  REVENUES_DELETE,
  REVENUES_DELETE_SUCCESS,
  REVENUES_DELETE_FAILURE
} from '../actionTypes';

const initialState = {
  isRequesting: false,
  isEditing: false,
  edited: false,
  created: false,
  received: 0,
  notReceived: 0,
  data: {},
  all: [],
  allById: {},
  error: ''
}

function calcReceived(data) {
  const response = data.reduce((sum, record) => {
    console.log(record)
    const value = parseFloat(record.value)
    if (record.received)
      return {
        ...sum,
        received: sum.received + value
      }
    return {
      ...sum,
      nReceived: sum.nReceived + value
    }
  }, { received: 0, nReceived: 0 })
  return response
}

export default function (state = initialState, action) {
  switch (action.type) {
    case REVENUES_GET_ALL: {
      return {
        ...state,
        isRequesting: true,
        error: ''
      }
    }
    case REVENUES_GET_ALL_SUCCESS: {
      const data = action.payload.docs.map(doc => ({ ...doc.data(), id: doc.id }))
      const info = calcReceived(data)
      console.log(info, "info")
      return {
        ...state,
        isRequesting: false,
        received: info.received,
        notReceived: info.nReceived,
        all: data,
        allById: keyBy(data, 'id')
      }
    }
    case REVENUES_GET_ALL_FAILURE: {
      return {
        ...state,
        isRequesting: false,
        error: action.payload
      }
    }
    case REVENUES_ADD: {
      return {
        ...state,
        isRequesting: true,
        error: ''
      }
    }
    case REVENUES_ADD_SUCCESS: {
      const data = [...state.all, action.payload]
      return {
        ...state,
        isRequesting: false,
        created: true,
        all: data,
        allById: keyBy(data, 'id')
      }
    }
    case REVENUES_ADD_FAILURE: {
      return {
        ...state,
        isRequesting: false,
        error: action.payload
      }
    }
    case REVENUES_EDIT: {
      return {
        ...state,
        isEditing: true,
        data: { ...action.payload },
        error: ''
      }
    }
    case REVENUES_UPDATE: {
      return {
        ...state,
        isRequesting: true,
        isEditing: false,
        error: ''
      }
    }
    case REVENUES_UPDATE_SUCCESS: {
      const data = state.all.filter(t => t.id !== action.payload.id)
      console.log(data)
      return {
        ...state,
        isRequesting: false,
        edited: true,
        all: [
          ...data,
          action.payload
        ]
      }
    }
    case REVENUES_UPDATE_FAILURE: {
      return {
        ...state,
        isRequesting: false,
        error: action.payload
      }
    }
    case REVENUES_DELETE: {
      return {
        ...state,
        isRequesting: true,
        error: ''
      }
    }
    case REVENUES_DELETE_SUCCESS: {
      const data = state.all.filter(t => t.id !== action.payload)
      return {
        ...state,
        isRequesting: false,
        all: data,
        allById: keyBy(data, 'id')

      }
    }
    case REVENUES_DELETE_FAILURE: {
      return {
        ...state,
        isRequesting: false,
        error: action.payload
      }
    }
    default:
      return state
  }
}