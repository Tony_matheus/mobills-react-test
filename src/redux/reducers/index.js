import { combineReducers } from 'redux';
import { firebaseReducer } from 'react-redux-firebase'
import { reducer as reduxFormReducer } from "redux-form";

import expenses from './expenses';
import revenues from './revenues';
import user from './user';

export default combineReducers(
  {
    expenses,
    revenues,
    user,
    form: reduxFormReducer,
    firebase: firebaseReducer
  }
)