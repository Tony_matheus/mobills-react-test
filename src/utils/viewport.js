const targetDesktop = {
  width: 1920,
  height: 1080
}

const targetHD = {
  width: 1024,
  height: 1080
}

function calculatePercentage(value, reference) {
  return value / reference * 100
}

const desktop = {
  vw: value => `${calculatePercentage(value, targetDesktop.width)}vw`,
  vh: value => `${calculatePercentage(value, targetDesktop.height)}vh`
}

const hd = {
  vw: value => `${calculatePercentage(value, targetDesktop.width)}vw`,
  vh: value => `${calculatePercentage(value, targetDesktop.height)}vh`
}

export default {
  desktop,
  dk: desktop, //alias
  hd
}