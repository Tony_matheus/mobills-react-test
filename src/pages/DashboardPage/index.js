import React from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import ExpensesList from '../../components/ExpensesList'
import FinancialMovementsForm from '../../components/FinancialMovementsForm'
import TitleBar from '../../components/TitleBar'

import { Title } from '../../components/_UI/Text/Title'
import Loading from '../../components/_UI/Loading'
import DataInfo from '../../components/DataInfo'
import CardInfo from '../../components/_UI/CardInfo'
import { Redirect } from '@reach/router'
import { vp } from '../../utils';

import { IfFirebaseAuthed } from "@react-firebase/auth";

export const Container = styled.div`
  min-height: 100vh;
  padding: 40px 0 47px 57px;
`

export const TitleWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 30px;
`
export const TitlePage = styled(Title)`
  color: white;
`

export const Wrapper = styled.div`
  display: flex;
  height: ${vp.dk.vw(693)};
  
  @media (max-width: 400px){
    flex-direction: column;
  }
`

export const Column = styled.div`
  flex-direction: column;
  ${({ hasMargin }) => hasMargin && "margin-right: 20px;"}
`


const DashboardPage = ({ isSignedIn }) => {
  const loading = useSelector(state => state.expenses.isRequesting)
  const loadingRevenue = useSelector(state => state.revenues.isRequesting)

  if (!isSignedIn)
    return (
      <Redirect
        noThrow
        to="/"
      />
    )

  return (
    <IfFirebaseAuthed >
      <Container>
        <Loading visible={loading || loadingRevenue} />
        <TitleBar />
        <Wrapper>
          <Column hasMargin>
            <FinancialMovementsForm />
            <CardInfo />
          </Column>
          <Column hasMargin>
            <ExpensesList collection={"expenses"} />
          </Column>
          <Column hasMargin>
            <ExpensesList collection={"revenues"} />
          </Column>
          <Column>
            <DataInfo />
          </Column>
        </Wrapper>
      </Container>
    </IfFirebaseAuthed>
  )
}

export default DashboardPage