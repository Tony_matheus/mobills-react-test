import React from 'react'
import Login from '../../components/Login'
import { IfFirebaseAuthed } from "@react-firebase/auth";
import { Redirect } from '@reach/router'

const LoginPage = ({ isSignedIn, user }) => {
  console.log(isSignedIn, "isSignedIn")
  console.log(user, "user")
  React.useEffect(() => {

  }, [isSignedIn, user])
  return (
    <>
      {!isSignedIn
        ? <Login />
        : <Redirect
            noThrow
            to="/dashboard"
        />
      }
    </>
  )
}

export default LoginPage